$(
    function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content"
        });
    });

$(function() {
    $(".preload").fadeOut(3000, function() {
        $(".content").fadeIn(1000);        
    });
});

$(document).ready(function () {
    $("#buttonTheme").click(
        function () {
            var style = window.getComputedStyle(document.getElementById('profilBody'));
            var bgColor = style.getPropertyValue('background-color');
            // window.alert(bgColor);

            if (bgColor == "rgb(107, 203, 225)") {
                $(this).html('Theme : Dark');
                $("body").css("background-color", "#2b2727");
                $("body").css("color", "white");
                $("button").css("background-color", "#7E7E7E");
                $("button").css("border-color", "#7E7E7E");
                $(".badge").css("background-color", "#7E7E7E");
                

                $("#accordion").css("background-color", "#808080");
                $(".accordJudul").css("color", "black");
                $(".accordJudul").css("background", "#808080");
                $(".accordIsi").css("background", "#A9B7C6");
                $(".accordIsi").css("color", "black");

            }
            else {
                $(this).html('Theme : Blue');
                $("body").css("background-color", "#6BCBE1");
                $("body").css("color", "black");
                $("button").css("background-color", "#033E76");
                $("button").css("border-color", "#033E76");
                $(".badge").css("background-color", "#033E76");
                

                $(".accordJudul").css("background", "#033E76");
                $(".accordJudul").css("color", "white");
                $("#accordion").css("background-color", "#6BCBE1");
                $(".accordIsi").css("background", "#1C8DB8");
                $(".accordIsi").css("color", "white");

            }



        }
    )
});
