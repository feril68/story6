from re import search

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
from .models import Status

from . import views
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
from selenium.webdriver.chrome.options import Options
import time



# Create your tests here.



class ProfUnitTest(TestCase):
	def test_url_home_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_url_addstatus_exist(self):
		response = Client().get('/addstatus')
		self.assertEqual(response.status_code, 302)

	def test_url_deletestatus_exist(self):
		response = Client().get('/deletestatus?')
		self.assertEqual(response.status_code, 200)
		
	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_using_addstatus_func(self):
		found = resolve('/addstatus')
		self.assertEqual(found.func, views.addstatus)

	def test_using_deletestatus_func(self):
		found = resolve('/deletestatus')
		self.assertEqual(found.func, views.deletestatus)

	def test_name_is_changed(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Feril</title>', html_response)
		self.assertIn('<h1>Hello, Apa kabar?</h1>', html_response)
		self.assertIn('<a href="http://tddferil.herokuapp.com/profile">My Profile</a>', html_response)

	def test_landing_html(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'index.html')

	def test_model_can_create_new_activity(self):
		new_activity = Status.objects.create(status="Test sebuah status")

		all_activities = Status.objects.all().count()
		self.assertEqual(all_activities, 1)

	def test_form_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form" method="POST" action="/addstatus">', html_response)

	def test_submit_button_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<button type="submit" id="submit">', html_response)

	def test_deleteform_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form id="form1" method="DELETE" action="/deletestatus">', html_response)	

	def test_delete_button_exist(self):
		request = HttpRequest()
		response = views.index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<button type="submit" id="delete">', html_response)	

	def test_url_profile_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)
	
	def test_using_profile_func(self):
		found = resolve('/profile')
		self.assertEqual(found.func, views.profile)

	def test_profile_html(self):
		response = Client().get('/profile')
		self.assertTemplateUsed(response, 'profile.html')
		
class fun_test(TestCase):
	def setUp(self):
       
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25)
		super(fun_test, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(fun_test, self).tearDown()

	def test_input_status_selenium(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')

		form = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit')

		form.send_keys('Test Input Status')
		submit.send_keys(Keys.RETURN)
		
	def test_status_exist(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		self.assertIn("Test Input Status", selenium.page_source)


	"""
	ini untuk challange lab
	"""

	"""
	ini untuk test layout
	"""
	def test_height_of_form_inputfield(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		
		form = selenium.find_element_by_id('id_status')
		size = form.size
		
		self.assertEqual(38, size['height'])


	def test_position_x_of_form_inputfield(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		
		form = selenium.find_element_by_id('id_status')
		loc = form.location
		
		self.assertEqual(0, loc['x'])

	def test_position_y_of_form_inputfield(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		
		form = selenium.find_element_by_id('id_status')
		loc = form.location
		
		self.assertEqual(144, loc['y'])

	"""
	ini untuk css property
	"""
	def test_color_of_thead_table(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		
		rgb = selenium.find_element_by_tag_name('thead').value_of_css_property('background-color')
		
		self.assertEquals("rgba(0, 0, 0, 0)", rgb)

	def test_h1_text_color(self):
		selenium = self.selenium
		selenium.get('http://tddferil.herokuapp.com/')
		
		rgb = selenium.find_element_by_tag_name('h1').value_of_css_property('background-color')
		
		self.assertEquals("rgba(0, 0, 0, 0)", rgb)


		

if __name__ == '__main__':
    unittest.main(warnings='ignore')
