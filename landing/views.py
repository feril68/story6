from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .forms import statusForm
from .models import Status

# Create your views here.

def index(request):
    form = statusForm

    response = {'form' : form, 'all' : Status.objects.all()}
    return render(request, 'index.html', response)

def profile(request):
    return render(request, 'profile.html')

response = {}
def addstatus(request):
	if request.method == 'POST':
		form = statusForm(request.POST or None)
		if form.is_valid():
			response['status'] = request.POST['status']
			status = Status(status=response['status'])

			status.save()
			return render(request, 'addstatus.html')

	else:
		return HttpResponseRedirect('')

def deletestatus(request):
	form= Status.objects.all()
	form.delete()
	return render(request, 'deletestatus.html')

def accor(request):
	return render(request, "accor.html")

