from re import search

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

class story10_test(TestCase):
    def test_url_prof_exist(self):
        response = Client().get('/story10/prof')
        self.assertEqual(response.status_code, 200)

    def test_url_validator_email_exist(self):
        response = Client().get('/story10/email_validator/')
        self.assertEqual(response.status_code, 200)

    def test_url_val_exist(self):
        response = Client().get('/story10/save_subscriber')
        self.assertEqual(response.status_code, 301)
        
    def test_url_list_datta_exist(self):
        response = Client().get('/story10/subscriberku')
        self.assertEqual(response.status_code, 301)

    def test_using_prof_func(self):
        found = resolve('/story10/prof')
        self.assertEqual(found.func, views.subscribe)

    def test_using_validator_func(self):
        found = resolve('/story10/email_validator/')
        self.assertEqual(found.func, views.email_validator)


    def test_using_val_func(self):
        found = resolve('/story10/subscriberku/')
        self.assertEqual(found.func, views.list_subs)