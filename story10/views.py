from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render,redirect
import urllib.request, json, requests
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from .forms import SubscriberForm
from .models import Subscriber

# Create your views here.


def subscribe(request):
    response = {
        'subscribe_form' : SubscriberForm
    }
    context={'subscribe':'active'}
    return render(request, "new.html", response)

def email_validator(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Salah Format Email cuyyy :D',
            'status':'ferguso'
        })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'Email udah aku pake cuyyyy :D',
            'status':'ferguso'
        })
        
    
    return JsonResponse({
        'message':'Email bisa dipakai cuyy :D',
        'status':'success'
    })

def save_subscriber(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            'message':'Semoga Berkah Cuyyy :D'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"Tak Semudah Itu Ferguso"
        }, status = 403)

def list_subs(request):
    response = {'all' : Subscriber.objects.all()}
    return render(request, 'muah.html', response)




