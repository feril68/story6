from django.db import models

class Subscriber(models.Model):
	name  = models.CharField(max_length = 60)
	email = models.EmailField(unique=True, db_index=True)
	password = models.CharField(max_length=30)

